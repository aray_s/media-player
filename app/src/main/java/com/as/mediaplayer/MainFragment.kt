package com.`as`.mediaplayer

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.`as`.mediaplayer.databinding.FragmentMainBinding

class MainFragment : Fragment() {

    private lateinit var binding: FragmentMainBinding
    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMainBinding.inflate(inflater, container, false)
        navController = findNavController()
        binding.playAudioBtn.setOnClickListener { getAudio.launch(getIntent("audio/*")) }
        binding.playVideoBtn.setOnClickListener { getVideo.launch(getIntent("video/*")) }
        return binding.root
    }

    private val getAudio =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) navController.navigate(
                MainFragmentDirections.actionToAudioFragment(result.data?.data.toString())
            )
            else failMessage()
        }

    private val getVideo =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) navController.navigate(
                MainFragmentDirections.actionToVideoFragment(result.data?.data.toString())
            )
            else failMessage()
        }

    private fun failMessage() =
        Toast.makeText(requireActivity(), "Load failed", Toast.LENGTH_SHORT).show()

    private fun getIntent(mediaType: String) = Intent(Intent.ACTION_GET_CONTENT).apply {
        type = mediaType
        addCategory(Intent.CATEGORY_OPENABLE)
        putExtra(Intent.EXTRA_MIME_TYPES, arrayOf(mediaType))
    }
}