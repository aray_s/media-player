package com.`as`.mediaplayer.audio

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.`as`.mediaplayer.R
import com.`as`.mediaplayer.databinding.FragmentAudioBinding

class AudioFragment : Fragment() {

    private val viewModel: AudioViewModel by viewModels()
    private lateinit var binding: FragmentAudioBinding
    private val args: AudioFragmentArgs by navArgs()
    private var firstTimePlayed = true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAudioBinding.inflate(layoutInflater)

        binding.btn.setOnClickListener {
            if (viewModel.isPlaying.value == true) viewModel.pause()
            else if (firstTimePlayed) {
                viewModel.initViewModel(requireContext(), Uri.parse(args.audioPath))
                viewModel.play()
                firstTimePlayed = false
            } else viewModel.play()
        }

        viewModel.uri.observe(viewLifecycleOwner) {
            it?.let {
                binding.filePath.text = getString(R.string.filepath, it.path)
            }
        }
        viewModel.isPlaying.observe(viewLifecycleOwner) {
            binding.btn.text = if (it) getString(R.string.pause) else getString(R.string.play)
        }

        return binding.root
    }


}
