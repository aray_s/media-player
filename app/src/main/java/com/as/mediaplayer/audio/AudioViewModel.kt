package com.`as`.mediaplayer.audio

import android.content.Context
import android.media.MediaPlayer
import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class AudioViewModel : ViewModel() {

    var uri : MutableLiveData<Uri> = MutableLiveData()
    var isPlaying = MutableLiveData<Boolean>().apply {postValue (false)}

    private lateinit var mediaPlayer: MediaPlayer
    private var position: Int = 0

    fun initViewModel(context: Context, _uri: Uri) {
        mediaPlayer = MediaPlayer().apply {
            setDataSource(context, _uri)
            prepare()
        }
        uri.value=_uri
    }

    fun play() {
        mediaPlayer.apply {
            seekTo(position)
            start()
        }
        isPlaying.value = true
    }

    fun pause() {
        mediaPlayer.apply {
            pause()
            position = currentPosition
        }
        isPlaying.value = false
    }

    override fun onCleared() {
        super.onCleared()
        mediaPlayer.release()
    }
}
