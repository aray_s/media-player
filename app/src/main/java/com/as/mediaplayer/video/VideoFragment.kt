package com.`as`.mediaplayer.video

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.`as`.mediaplayer.R
import com.`as`.mediaplayer.databinding.FragmentVideoBinding

class VideoFragment : Fragment() {

    private val viewModel: VideoViewModel by viewModels()
    private lateinit var binding: FragmentVideoBinding
    private val args: VideoFragmentArgs by navArgs()
    private var firstTimePlayed = true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentVideoBinding.inflate(layoutInflater)

        binding.videoView.setOnPreparedListener { mediaPlayer ->
            viewModel.setMediaPlayer(mediaPlayer)
            if (viewModel.isPlaying.value == true) viewModel.play()
        }

        binding.videoView.setOnCompletionListener {
            viewModel.pause()
            viewModel.currentTime.value=0
        }

        binding.btn.setOnClickListener {
            if (viewModel.isPlaying.value == true) viewModel.pause()
            else if (firstTimePlayed) {
                viewModel.initViewModel(requireContext(), Uri.parse(args.videoPath))
                viewModel.play()
                firstTimePlayed = false
            } else viewModel.play()
        }

        binding.seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (fromUser) {
                    binding.videoView.seekTo(progress)
                    viewModel.currentTime.value = progress
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
        })


        viewModel.uri.observe(viewLifecycleOwner) { it?.let { binding.videoView.setVideoURI(it) } }

        viewModel.currentTime.observe(viewLifecycleOwner) {
            binding.seekBar.progress = it
            binding.currentTime.text = formatTime(it)
        }

        viewModel.duration.observe(viewLifecycleOwner) {
            binding.seekBar.max = it
            binding.duration.text = formatTime(it)
        }

        viewModel.isPlaying.observe(viewLifecycleOwner) {
            binding.btn.text = if (it) getString(R.string.pause) else getString(R.string.play)
        }

        return binding.root
    }

    private fun formatTime(timeInMillis: Int): String {
        val seconds = (timeInMillis / 1000) % 60
        val minutes = (timeInMillis / (1000 * 60)) % 60
        return String.format("%02d:%02d", minutes, seconds)
    }

    override fun onPause() {
        viewModel.onChangeConfig()
        super.onPause()
    }
}
