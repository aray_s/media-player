package com.`as`.mediaplayer.video

import android.content.Context
import android.media.MediaPlayer
import android.net.Uri
import android.os.Looper
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class VideoViewModel : ViewModel() {

    val uri: MutableLiveData<Uri> = MutableLiveData<Uri>()
    val isPlaying = MutableLiveData<Boolean>().apply {postValue (false)}
    val currentTime = MutableLiveData<Int>().apply { postValue(0) }
    val duration: MutableLiveData<Int> = MutableLiveData<Int>()

    private lateinit var mediaPlayer: MediaPlayer

    private var runnable: Runnable = Runnable {}
    private var handler = android.os.Handler(Looper.getMainLooper())

    fun initViewModel(context: Context, _uri: Uri) {
        mediaPlayer = MediaPlayer().apply {
            setDataSource(context, _uri)
            prepare()
        }
        uri.value = _uri
        duration.value = mediaPlayer.duration
    }

    fun play() {
        mediaPlayer.apply {
            seekTo(currentTime.value?.toLong() ?: 0, MediaPlayer.SEEK_CLOSEST)
            start()
        }
        isPlaying.value = true
        runnable = Runnable {
            mediaPlayer.let {
                currentTime.postValue(it.currentPosition)
            }
            handler.postDelayed(runnable, SECOND.toLong())
        }
        handler.postDelayed(runnable, SECOND.toLong())
    }

    fun pause() {
        mediaPlayer.pause()
        isPlaying.value = false
        handler.removeCallbacks(runnable)
    }

    fun setMediaPlayer(player: MediaPlayer) {
        mediaPlayer.release()
        mediaPlayer = player
        duration.value = player.duration
    }

    fun onChangeConfig() {
        mediaPlayer.pause()
        handler.removeCallbacks(runnable)
    }

    override fun onCleared() {
        super.onCleared()
        handler.removeCallbacks(runnable)
        mediaPlayer.release()
    }

    companion object {
        const val SECOND = 1000
    }
}
